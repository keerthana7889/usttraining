package com.ust.model;

public class Employee {
	

	//instance variable
	private int id;
	private String name;
	private double  salary;
	 
	//default cons
	public Employee() {
		super();
		System.out.println("used to initialize the default values forr instance variables");
		// TODO Auto-generated constructor stub
	}
	
	
	//paramaterised connstruc
	public Employee(int id, String name, double salary) {
		
		super();
		System.out.println("used to initialize set values for instance variables");
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

	//getter and setter
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getSalary() {
		return salary;
	}


	public void setSalary(double salary) {
		this.salary = salary;
	}

	//override to string
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
}		



package com.ust.mapexample;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;

import com.ust.dao.EmployeeDaoImple;
import com.ust.model.Employee;

public class EmployeeMain {
	public static void main(String[] args) throws Exception {
	BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
	EmployeeDaoImple daoImple=new EmployeeDaoImple();
	String str="";
	do {
		System.out.println("Press\n1.Insert\n2.Search");
		int choice=Integer.parseInt(reader.readLine());
		switch (choice) {
		case 1:
			Employee employee=new Employee();
			System.out.println("Id");
			int id=Integer.parseInt(reader.readLine());
			employee.setId(id);
			employee.setName(reader.readLine());
			employee.setSalary(Double.parseDouble(reader.readLine()));
			boolean status=daoImple.insertEmployeeDetails(id, employee);
			if(status) {
				System.out.println("Data Inserted Successfully");
			}
			break;
		case 2:
			Map<Integer, Employee> emap = daoImple.getAllEmployeeDetails();
			System.out.println("Enter Id to serach");
			int serachKey=Integer.parseInt(reader.readLine());
			if(emap.containsKey(serachKey)) {
				Employee emp = emap.get(serachKey);
				System.out.println(emp.getId() +"--" + emp.getName()+"--"+emp.getSalary());
			}
			else {
				System.out.println("Employee is not found");
			}
			break;
		default:
			break;
		}
		System.out.println("Do You Want to cont press y");
		str=reader.readLine();
		
	}while(str.equalsIgnoreCase("y"));
	
	
}

}



package com.ust.mapexample;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapExample2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		Map<Integer,Integer> marks=new HashMap<Integer,Integer>();
		marks.put(11, 88);
		marks.put(12, 55);
		marks.put(13, 67);
		marks.put(14, 90);


		for(Entry<Integer,Integer> student :marks.entrySet()) {
			if(student.getValue()<60) {
				System.out.println(student.getKey());
			}

		}
		int sumE=0,sumO=0;
		for(Entry<Integer,Integer> stu : marks.entrySet()) {
			if(stu.getKey()%2==0) {
				sumE=sumE+stu.getValue();

			}
			else
			{
				sumO=sumO+stu.getValue();
			}

		}
			System.out.println("even sum is "+ sumE);
			System.out.println("odd sum is "+ sumO);
		}
	}
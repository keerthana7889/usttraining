package com.ust.mapexample;

import java.util.Map.Entry;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Map<String,String> hmap=new HashMap<String,String>();
		hmap.put("UST101", "keert");
		hmap.put("UST1011", "keerthana");
		hmap.put("UST1012", "Hema");
		hmap.put("UST1013", "joy");
		hmap.put(null, null);
		hmap.put(null, "vijay");
		hmap.put("234", null);
		hmap.put("UST1011", "keerthana G"); // no duplicate it will replace
		
		System.out.println(hmap);
		System.out.println(hmap.containsKey("UST1013"));
		System.out.println(hmap.containsValue("keert"));
		String  x=hmap.get("UST1012");
		System.out.println(x);
		Collection<String> values= hmap.values();
		System.out.println("Values "+ values);
		Set<String> keys=hmap.keySet();
		System.out.println("Keys "+keys);
		  
		String remove=hmap.remove(null);
		System.out.println("Removed value "+ remove);
		
		System.out.println(hmap);
		 Set<Entry<String,String>> entrySet = hmap.entrySet();
		 System.out.println(entrySet);
		 
		for(Entry<String,String> map : hmap.entrySet()) {
			System.out.println(map.getKey()+ " ---" + map.getValue());
		}
		
	}

}
 
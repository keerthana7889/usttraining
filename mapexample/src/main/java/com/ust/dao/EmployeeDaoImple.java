package com.ust.dao;

import java.util.HashMap;
import java.util.Map;

import com.ust.model.Employee;

public class EmployeeDaoImple {


	Map<Integer, Employee> employeeMap = new HashMap<Integer, Employee>();

	public boolean insertEmployeeDetails(int id, Employee employee) {
		Employee emp = employeeMap.put(id, employee);
		if (emp != null) {
			return true;
		}
		return false;
	}

	public Map<Integer, Employee> getAllEmployeeDetails() {
		
		return employeeMap;
	}

}

package com.ust.dao;

import java.util.Map;

import com.ust.model.Employee;

public interface EmployeeDao {
	// Key Employee Id Value Employee Object
		boolean insertEmployeeDetails
		(int id,Employee employee);
		
		Map<Integer,Employee> getAllEmployeeDetails();

	}



package com.ust.Dao;

import java.util.Map;

import com.ust.model.ClientDetails;


public interface ClientDetailsDao {
	
	boolean insertClientDetails( String PassportNumber,ClientDetails client);
	Map<String,ClientDetails> getAllClientDetails();
	

}


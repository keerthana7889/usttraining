package com.ust.model;

public class ClientDetails {
 private String clientName;
 private String email;
 private String passportNumber;

 
 public ClientDetails(String clientName, String email, String passportNumber) {
	super();
	this.clientName = clientName;
	this.email = email;
	this.passportNumber = passportNumber;
}


public ClientDetails() {
		super();
		// TODO Auto-generated constructor stub
	}



@Override
public String toString() {
	return "ClientDetails [clientName=" + clientName + ", email=" + email + ", passportNumber=" + passportNumber + "]";
}


public String getClientName() {
	return clientName;
}


public void setClientName(String clientName) {
	this.clientName = clientName;
}


public String getEmail() {
	return email;
}


public void setEmail(String email) {
	this.email = email;
}


public String getPassportNumber() {
	return passportNumber;
}


public void setPassportNumber(String passportNumber) {
	this.passportNumber = passportNumber;
}
	 
 
 
 
 
 
 
 
}

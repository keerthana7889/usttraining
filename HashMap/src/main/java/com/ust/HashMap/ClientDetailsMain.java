package com.ust.HashMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Scanner;

import com.ust.Dao.ClientDetailsDaoImpl;
import com.ust.model.ClientDetails;

public class ClientDetailsMain {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
		ClientDetailsDaoImpl daoimp=new ClientDetailsDaoImpl();
		System.out.println("Enter the number of Clients");
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		int count=1;
		for(int i=1;i<=num;i++) {
			System.out.println("Enter the details of client"+ count);
			ClientDetails client=new ClientDetails();
			client.setClientName(reader.readLine());
			client.setEmail(reader.readLine());
			String passportNumber=reader.readLine();
			boolean status=daoimp.insertClientDetails(passportNumber, client);
			count++;
			
		}
		Map<String, ClientDetails> map1=daoimp.getAllClientDetails();
		System.out.println("Enter the passport number of the client to be serached");
		String searchKey=reader.readLine();
		System.out.println("Client Details");
		if(map1.containsKey(searchKey)) {
			ClientDetails cli=map1.get(searchKey);
			System.out.println(cli.getClientName() +" "+ cli.getEmail() +" "+ searchKey);
			}
		else
		{
			System.out.println("Client not found");
		}
		
	}

}

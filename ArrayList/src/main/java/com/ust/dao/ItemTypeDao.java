package com.ust.dao;

import java.util.List;

import com.ust.model.ItemType;

public interface ItemTypeDao {
  boolean insertItemDetails(ItemType item);
  
  List<ItemType> getAllItemDetails();
}

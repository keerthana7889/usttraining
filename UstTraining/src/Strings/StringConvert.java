package Strings;

public class StringConvert {

	public static void main(String[] args) {
		String firstname="keerthana";
		char[] name=firstname.toCharArray();
		for (char w:name) {
			System.out.println(w);
		}
		System.out.println("size of an character array is "+name.length);
		System.out.println("string in reverse order is ");
		for(int i=name.length-1;i>=0;i--) {
			System.out.println(name[i]);
		}
		String stringvalue="";
		for(int i=name.length-1;i>=0;i--) {
			stringvalue=stringvalue.concat(String.valueOf(name[i]));
		}	
		System.out.println("concatenated string is "+stringvalue);
	
	}
}

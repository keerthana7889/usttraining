package Strings;

public class StringBuilderEx {

	
		public static void main(String[] args) {
			StringBuilder sb=new StringBuilder("Keerthana");
			System.out.println("entered string is "+sb);
			String name="StackRoute";
			StringBuilder st=new StringBuilder(name);
			System.out.println("my organization name is "+st);
			
			StringBuilder completeWord=sb.append(name);
			System.out.println(completeWord);
			
			
			//to find the length of string buffer
			System.out.println("length of a string buffer is "+st.length());
			
			//to perform reverse operation
			StringBuilder reverseString=st.reverse();
			System.out.println("the reversed string is "+reverseString);
			
			//to insert character at given index
			StringBuilder completeWord1=new StringBuilder("my name is Joen");
			StringBuilder insertString=completeWord1.insert(3, "joy");
			System.out.println(insertString);
			
			StringBuilder deleteString=insertString.delete(3, 10);
			System.out.println(deleteString);
			
			System.out.println("character at 5th index is "+deleteString.charAt(5));
		}
	}
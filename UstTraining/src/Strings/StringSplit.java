package Strings;

public class StringSplit {

	public static void main(String[] args) {   
	String name="keerthana Govindarajulu";
	String[] nameSplit=name.split("\\s");
	for(String w:nameSplit) {
		System.out.println(w);
	}
	
	name="Hello-world";
	nameSplit=name.split("\\-");
	for(String w:nameSplit) {
		System.out.println(w);
	}

	name="Hello-world Welcome";
	nameSplit=name.split("[-\\s]");
	for(String w:nameSplit) {
		System.out.println(w);
	}

	String completeString="my name is keerthana";
	completeString=completeString.toUpperCase();
	System.out.println("converted to "+completeString);
	

	String completeString1="KEERTHANA";
	completeString1=completeString1.toLowerCase();
	System.out.println("converted to "+completeString1);
}
	}

	

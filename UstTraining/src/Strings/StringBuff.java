package Strings;

public class StringBuff {

	public static void main(String[] args) {
     StringBuffer sb= new StringBuffer("Keerthana");
     System.out.println("entered string is "+sb);
     String name="welcome";
     StringBuffer st=new StringBuffer(name);
     System.out.println("another string is "+  st);
     
     StringBuffer completeWord=sb.append(st);
     System.out.println(completeWord);
     
     System.out.println("length of string buffer is "+completeWord.length());
     
     StringBuffer reverseString=completeWord.reverse();
     System.out.println("reverse string is "+reverseString);
       
     StringBuffer completeWord1= new StringBuffer("my name is keerthana");
     StringBuffer insertString= completeWord1.insert(11,"G ");
     System.out.println(insertString);
     
     
     StringBuffer deleteString=insertString.delete(3,7);
     System.out.println(deleteString);
     
     System.out.println("character at 5th index is "+deleteString.charAt(5));
     
     
     
     
     
	}

}

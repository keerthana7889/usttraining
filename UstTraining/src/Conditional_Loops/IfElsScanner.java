package Conditional_Loops;

import java.util.Scanner;

public class IfElsScanner {

	public static void main(String[] args) {
		System.out.println("please enter an value of a");
		int a;
		Scanner sc=new Scanner(System.in);
		a=sc.nextInt();
		if (a%2==0) {
			System.out.println("It is even");
		}
		else {
			System.out.println("It is odd");
		}
		sc.close();
	}
	

}

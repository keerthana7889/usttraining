package Conditional_Loops;

import java.util.Scanner;

public class IfElseLadder {
	public static void main(String[] args) {
		System.out.println("please enter an value of a");
		int a;
		Scanner sc=new Scanner(System.in);
		a=sc.nextInt();
		if (a==1) {
			System.out.println("It is Monday");
		}
		else if (a==2) {
			System.out.println("It is Tuesday");
		}else if(a==3) {
			System.out.println("It is wednesday");
		}
		else {
			System.out.println("Invalid input");
		}
		sc.close();
	}
	

}

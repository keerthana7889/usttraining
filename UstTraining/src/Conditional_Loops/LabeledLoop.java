package Conditional_Loops;

public class LabeledLoop {

	public static void main(String[] args) {
		loop1:
			 for(int i=0;i<3;i++) {
				 for(int j=0;j<3;j++) {
					 if(j==2 &&i==2) {
						 break loop1;
					 }
				 
					 System.out.println(i+ " "+j);
				 }
	} System.out.println("***");
		loop2:
			 for(int i=0;i<3;i++) {
				 for(int j=0;j<3;j++) {
					 if(j==2 ) {
						 break loop2;
					 }
				 
					 System.out.println(i+ " "+j);
				 }
	}
	}
}

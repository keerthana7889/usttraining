package OOPS;

public class MethodOverloading {
	public void learning() {
		System.out.println("you are in safe hands  for learning");
	}
	public void learning(int duration) {
		System.out.println("you are in safe hands  for"+duration+" learning");
	}
	public void learning(String coursename ) {
		System.out.println("you are  learning "+coursename);
	}
	public void learning(int duration,String coursename) {
		System.out.println("you are enrolled for"+coursename+"course for "+duration+ "learning");
	}
	

}

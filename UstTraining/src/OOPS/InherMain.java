package OOPS;

public class InherMain  {

	public static void main(String[] args) {
		System.out.println("Perform operation on parent class");
		InheriDemo2 demo=new InheriDemo2();
		demo.balanceEnquiry();
		demo.amountTransfer();
		
		System.out.println("perform operation on sub class");
		//InheriDemo2 demo1= new InheriDemo2();
		demo.quicktransfer();

	}

}

package Array;

public class CloningArray {

	
		public static void main(String[] args) {
			int[] integer= {23,53,12,89,11};
			System.out.println("array before cloning");
			for(int i=0;i<integer.length;i++) {
				System.out.println(integer[i]+" ");
			}
			System.out.println();
			int[] integer1=integer.clone();
			System.out.println("array after cloning");
			for(int i=0;i<integer.length;i++) {
				System.out.println(integer1[i]+" ");
		}
	}}


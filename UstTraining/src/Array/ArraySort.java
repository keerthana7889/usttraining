package Array;

import java.util.Arrays;

public class ArraySort {

	public static void main(String[] args) {
		int[] integer= {23,53,12,89,11};
		System.out.println("array before sorting");
		for(int i:integer) {
			System.out.println(i);
		}
System.out.println("array after sorting");
Arrays.sort(integer);
for(int i:integer) {
	System.out.println(i);
}
String[] string= {"man","women","children"};
for (int i=0;i<string.length;i++) {
	System.out.println(string[i]+ " ");
	
}
System.out.println();
System.out.println("after sorting");
Arrays.sort(string);
for(int i=0;i<string.length;i++) {
	System.out.println(string[i]+ " ");
}
	}

}

package Array;

import java.util.Scanner;

public class TwoDiArr {
	public static void main(String[] args)	{
		Scanner sc= new Scanner(System.in);
		System.out.println("specify number of rows");
		int rowcount=sc.nextInt();
		System.out.println("specify number of columns");
		int columncount = sc.nextInt();
		System.out.println("please enter the values of first array");
		int[][] integer1=new int[rowcount][columncount];
		for(int i=0;i<rowcount;i++) {
			for(int j=0;j<columncount;j++) {
				integer1[i][j]=sc.nextInt();
			}
		}
		System.out.println("please enter the values of second array");
		int[][] integer2=new int[rowcount][columncount];
		for(int i=0;i<rowcount;i++) {
			for(int j=0;j<columncount;j++) {
				integer2[i][j]=sc.nextInt();
			}
		}
		int[][] integer3=new int[rowcount][columncount];
		for(int i=0;i<rowcount;i++) {
			for(int j=0;j<columncount;j++) {
				integer3[i][j]=integer1[i][j]+integer2[i][j];
			}
		}
		System.out.println("values after adding");
		for(int i=0;i<rowcount;i++) {
			for( int j=0;j<columncount;j++) {
				System.out.println(integer3[i][j]+"\t");
			}
			System.out.println();
			
		}
		sc.close();
	}

}

package DateOperation;

public class StaticKeyEx {
	public  static int add=4;
	 
	static{
		System.out.println("i am inside static block");
	}

	public static void main(String[] args) {
		int  resultData=addOperation(5);
		System.out.println(resultData);
	}
	public static int addOperation(int number)
	{
		int result=add+number;
		return result;
	}
}

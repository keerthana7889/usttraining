package DateOperation;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateTimeEx {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        LocalDateTime datetime=LocalDateTime.now();
        System.out.println("current date & time is "+datetime);
        
        
        LocalDateTime formatteddatetime=LocalDateTime.of(2021, 12, 3, 2, 34);
        System.out.println("formatted date and time is "+formatteddatetime);
        
        ZoneId paris=ZoneId.of("Europe/Paris");
        ZonedDateTime parisdatetime=ZonedDateTime.now(paris);
        System.out.println("paris time  is"+parisdatetime);
        
        ZonedDateTime parisdatetime1=ZonedDateTime.of(LocalDateTime.of(2021,10, 20, 20, 10, 30),paris);
        System.out.println(parisdatetime1);
        
        ZoneId newyork=ZoneId.of("America/New_York");
        ZonedDateTime newyorkdatetime=ZonedDateTime.now(newyork);
        System.out.println("newyork time  is"+newyorkdatetime);
        
         Instant instantTime=Instant.now();
         System.out.println("instant date time is "+instantTime);
         
        
        
	}

}

package DateOperation;

import java.time.LocalDate;
import java.time.Month;

public class DateOp {

	public static void main(String[] args) {
		LocalDate localDate=LocalDate.now();
		System.out.println("current date on the machine is "+localDate);
		
		LocalDate localDateFormat=LocalDate.of(2021, 10, 19);
		System.out.println("date format is "+localDateFormat);
		
		
		LocalDate localDateFormat1=LocalDate.of(2021, Month.OCTOBER, 19);
		System.out.println("date format is "+localDateFormat1);
	}

}

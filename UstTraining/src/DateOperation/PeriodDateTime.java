package DateOperation;

import java.time.LocalDate;
import java.time.Period;

public class PeriodDateTime {

	public static void main(String[] args) {
		LocalDate startDate=LocalDate.of(2019, 10, 8);
		LocalDate enddate=LocalDate.of(2021, 10, 20);
		Period perioddate=Period.between(startDate, enddate);
		int days=perioddate.getDays();
		System.out.println("differnce in days "+days);
		int month=perioddate.getMonths();
		System.out.println("differnce in months is "+month);
		int  year=perioddate.getYears();
		System.out.println("differnce in years is "+year);
				}
}

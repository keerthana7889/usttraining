package DateOperation;

import java.time.LocalTime;

public class TimeOp {

	public static void main(String[] args) {
		LocalTime localtime=LocalTime.now();
	System.out.println("current time is "+localtime);	
	
	LocalTime formattedTime=LocalTime.of(9, 23);
	System.out.println("formatted time is "+formattedTime);
	
	LocalTime formattedTime1=LocalTime.of(6, 35, 56);
	System.out.println("formatted time is "+formattedTime1);
	
	
	}

}

package com.ust.employeedetail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import com.ust.dao.EmployeeDaoImp;
import com.ust.model.Employee;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	
    	BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
  EmployeeDaoImp daoimp=new  EmployeeDaoImp();  	
  
    	do {
    		
    	System.out.println("Press 1.insert \n 2.List Employee \n 3.exit");
    	int choice=Integer.parseInt(reader.readLine());
    	switch(choice) {
    	case 1:
    		Employee emp= new Employee();
    		System.out.println("id");
    		emp.setId(Integer.parseInt(reader.readLine()));
    		
    	
    		 System.out.println("name");
             emp.setName(reader.readLine());
    		
    		System.out.println("salary");
            emp.setSalary(Double.parseDouble(reader.readLine()));
    		boolean status = daoimp.insertEmployeeDetail(emp);
    		if(status)
    		{
    			System.out.println("data added");
    		}
    		else {
    			System.out.println("data not added");
    		}
    		break;
    	case 2:
    		//print emp details
    		List<Employee> list= daoimp.getAllEmployeeDetails();
    		if(list.isEmpty()) {
    			System.out.println("list is empty");
    		}else
    		{
    			for(Employee employee: list) {
    				System.out.println(employee.getId()+"  "+ employee.getName()+"--"+employee.getSalary());
    			
    			}
    				
    		}
    		
    		break;
    		default:
    			System.out.println("thank you");
    			System.exit(0);
    	}
    	}while(true);
    	
    	
    }
}

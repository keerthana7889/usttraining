package com.ust.dao;

import java.util.List;

import com.ust.model.Employee;

public interface EmployeeDao {
	//DAO-  data access object ---data related store,retrive,manipulate
	//abstract methods-does not contain
	
	boolean insertEmployeeDetail(Employee emp);
    
	 List<Employee> getAllEmployeeDetails();
	 
	 
	 
}
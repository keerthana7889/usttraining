package com.ust.dao;

import java.util.ArrayList;
import java.util.List;

import com.ust.model.Employee;

public class EmployeeDaoImp implements EmployeeDao{

	List<Employee> employeelist=new ArrayList<Employee>();
	//store employee details  in array list
	public boolean insertEmployeeDetail(Employee emp) {
		boolean add= employeelist.add(emp);
		return add;
	}

	//retrieve the employee details
	public List<Employee> getAllEmployeeDetails() {
		return employeelist;
	}

}

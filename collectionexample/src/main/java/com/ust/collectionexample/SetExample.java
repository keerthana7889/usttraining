package com.ust.collectionexample;


import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SetExample {
	public static void main(String[] args) {
		HashSet<String> city=new HashSet<String>();
		city.add(null);
		city.add("chennai");
		city.add("goa");
		city.add("mumbai");
		city.add("chennai");
		//city.add();
		System.out.println(city);
		
		boolean  status=city.isEmpty();
		System.out.println(status);
		
		boolean pre=city.contains("goa");
		System.out.println(pre);
		
		boolean remove= city.remove("goa");
		if(remove) {
			System.out.println("city removed");
		}else
		{
			System.out.println("city not present");
		}
		
		System.out.println(city);
	}

}

package com.ust.collectionexample;

import java.util.ArrayList;
import java.util.List;

public class ArrayListEx {	
	public static void main(String[] args) {
	int[] marks=new int[] {13,16,56,78,90};  //20bytes
	marks[1]=0;
	
	//ArrayList
	//Declare ArrayList
	ArrayList<Integer> list=new ArrayList<Integer>();
	list.add(100); // 4 bytes
	list.add(200); // 8 bytes
	list.add(300);
	list.add(400);
	list.add(500);//20 bytes
	list.add(100);//duplicate
	list.add(null);//with null
	//list.add("jgc") compile time error  
	
	int size=list.size();
	System.out.println("size of an ArrayList "+ list.size());
	int value=list.get(0);
	System.out.println("Element at index " + list.get(0));
	
	list.set(0, 333);
	System.out.println("After set" +list);
	list.add(1,444);
    System.out.println("After add" +list);
    
    System.out.println("check value is present or not");
    if(list.contains(444)) {
    	System.out.println("444 is found");
    }else
    {
    	System.out.println("444 is not ");
    }
    int index=list.indexOf(444);
    System.out.println("index of 444 "+ index);
    System.out.println("Last Index  of  100 "+ list.lastIndexOf(100));
    
    if(list.isEmpty()) {
    	System.out.println("list is empty");
    }
    
    List<Integer> sublist=list.subList(0, 5);
    System.out.println(sublist);
    
	System.out.println(" Size of arraylist "+list.size());
	System.out.println(list.get(0));
	System.out.println(list.get(1));
	System.out.println(list.get(2));
	System.out.println(list.get(3));
	System.out.println(list.get(4));
	
	
	//print ArrayList
	//for each Loop
	for(Integer no: list) {
		System.out.println("--"+no);
		
	}
	
	
}
}



package com.ust.collectionexample;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListSearch {

	public static void main(String[] args) {
	
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the size of an array");
		int size=sc.nextInt();
		ArrayList<Integer> list=  new ArrayList<Integer>();
		for(int i=0;i<size;i++)
		{
			System.out.println("Enter the number");
			list.add(sc.nextInt());
		} 
		System.out.println(list);
		System.out.println("Enter number to search");
		int searchkey=sc.nextInt();
		int count=0;
		for(int i=0;i<list.size();i++) {
			if(list.get(i)==searchkey) {
				System.out.println(searchkey + " is present at index " + i);
				count++;
			}
		}
		if(count==0) {
			System.out.println(searchkey + " is not found " );
		}else
		{
			System.out.println("element occured " + count + " times");
		}
	}}
		/*if(list.contains(searchkey)) {
			System.out.println(searchkey + " is present at  index "+ list.indexOf(searchkey));
			
		}else
		{
			System.out.println(searchkey +" is not found");
		} 
		
		
	}

}*/

package com.ust.collectionexample;

import java.util.LinkedList;
import java.util.Stack;

public class LinkedListEx {

	public static void main(String[] args) {
		LinkedList<Integer> linked=new LinkedList<Integer>();
		linked.add(122);
		linked.add(3445);
		linked.add(345);
		linked.add(34);
		for(Integer x:linked) {
			System.out.println(x);
		}
		linked.push(476);
		System.out.println(linked);
		Integer firstElement=linked.pop();
		System.out.println(firstElement);
		System.out.println(linked);
		
		System.out.println(linked.peek());
		System.out.println(linked);
		
		 
		 System.out.println("Last Element  "+ linked.peekLast());
		 
		 System.out.println(linked.poll());//remove and retrieve the first element
		 System.out.println(linked);
		
		 System.out.println(linked.pollLast());
		 System.out.println(linked);
		
		 linked.addFirst(234);
		 linked.addLast(000);
		 
		 System.out.println(linked.get(3)+ "--" + linked.getFirst() +"--"+ linked.getLast() );
		linked.offer(6666);
		linked.offerFirst(11111);
		linked.offerLast(8888);
		
		System.out.println(linked);
		
		
		Stack<String> city=new Stack<String>();
		city.add("chennai");
		city.add("Goa");
		city.add("mumbai");
		city.add("delhi");
		city.push("Goa");
		System.out.println(city);
		city.pop();
		System.out.println(city);
		
		for(String x:city) {
			System.out.println(x);
		}
		
		
	}
}

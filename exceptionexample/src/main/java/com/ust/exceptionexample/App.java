package com.ust.exceptionexample;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	//exception
    	int a=100,b=20,result=0;
    	int arr[]=new int[] {1,2,3,4,5};
    	Scanner scan= new Scanner(System.in);
    	String str=null;
    	try {
    		result=a/b;
    		
    	}
    	catch(Exception e) {
    		System.out.println(e.getMessage());
    		System.out.println("divide by zero not possible");
    	}
    	System.out.println("Value of result "+ result);
    	try {
    		System.out.println(str.toLowerCase());
    	}
    	catch(Exception ab) {
    		System.out.println(ab.getMessage());
    	}
    	try {
    		System.out.println(arr[6]);
    	}
    	catch(ArrayIndexOutOfBoundsException e)
    	{
    		System.out.println(e.getMessage());
    	}
    	try {
    		int no=scan.nextInt();
    		System.out.println(no);
    		
    	}catch(InputMismatchException ex) {
    		System.out.println(ex.getMessage() +" enter only numbers");
    	}
    	try {
    		int no2=Integer.parseInt("23abc");//convert to integer
    		System.out.println(no2);
    	}
    	catch(NumberFormatException e) {
    		System.out.println(e.getMessage() + " character cannot convert  to numbers");
    		
    	}
    	
    	System.out.println( "Line1" );
    }
}

package com.ust.junitexample;


import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FactorialTest {
	Factorial fact=null;
	@Before
	public void setUp()
	{
		fact= new Factorial();
		System.out.println("before will be executed first for each test method ");
	}
   @Test
	public void findFactorialForZeroThenReturnOne() {
		//Factorial fact=new Factorial();
	   System.out.println("method 1");
		int result=fact.findFactorial(0);
		assertEquals(1,result);
	}
	@Test
	public void findFactorialForOneThenReturnOne() {
		//Factorial fact=new Factorial();
		System.out.println("method 2");
		int result=fact.findFactorial(1);
		assertEquals(1,result);
}
	@Test
	public void findFactorialForValidPositiveInteger() {
	//Factorial fact=new Factorial();
		System.out.println("method 3");
	int result=fact.findFactorial(5);
	assertEquals(120,result);
	assertEquals(720,fact.findFactorial(6));
}
	@Test
	public void findFactorialForInvalidNegativeInteger() {
		System.out.println("method 4");
		//Factorial fact=new Factorial();
		int result=fact.findFactorial(-5);
		assertEquals(-1,result);
		
	}
	@After
	public void teardown() {
		fact=null;
		System.out.println("After will be executed for each method");
	}
	
	
	
}
